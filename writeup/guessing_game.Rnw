\documentclass[12pt]{article}

\usepackage{algorithm2e}
\usepackage{algorithmic}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{booktabs}
\usepackage{dcolumn}
\usepackage{dcolumn}
\usepackage{epstopdf}
\usepackage{fourier}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{nag}
\usepackage{natbib}
\usepackage{pdflscape}
\usepackage{rotating}
\usepackage{setspace}
\usepackage{tabularx} 
\usepackage{tikz} 
\usepackage{url}
\usepackage{ifthen}
\usepackage{verbatim} 

\hypersetup{colorlinks = TRUE,citecolor=blue,linkcolor=red,urlcolor=black}
\DeclareMathOperator*{\argmax}{arg\,max}
\newcommand{\starlanguage}{Significance indicators: $p \le 0.05:*$,
  .$p \le 0.01:**$ and $p \le .001:***$.}  
\newtheorem{proposition}{Proposition}
\newtheorem{assumption}{Assumption}
\newtheorem{lemma}{Lemma}

\begin{document}

\title{The Dot-Guessing Game: \\
  A ``Fruit Fly'' Task for Computer Supported Work Experiments}

\author{John J. Horton\\NYU Stern}

%\author{
% 1st. author
%       \alignauthor John J. Horton\\
%       \affaddr{Harvard University}\\
%       \affaddr{383 Pforzheimer Mail Center}\\
%       \affaddr{56 Linnaean Street}\\
%       \affaddr{Cambridge, Massachusetts 02138}\\
%       \email{john.joseph.horton@gmail.com}
%}

\date{}
\maketitle

\begin{abstract} 
 One common form of computer supported work has participants provide
 assessments or offer independent sources of information that must be
 aggregated. To help study this kind of work, I propose a task that
 has a number of properties that make it useful for empirical
 research. The task itself is simple: subjects are asked to guess the
 number of dots in a computer-generated image. The task is a CAPTCHA
 and and has an objective solution and yet subjects cannot simply look
 up the answer. To demonstrate its value, two experiments were
 conducted using the the task. Across both experiments, the ``crowd''
 performed well, with the arithmetic mean guess besting most
 individual guesses. However, subjects displayed well-known behavioral
 patterns relevant to the design of human computation / crowdsourcing
 systems. In particular, subjects were susceptible to anchoring and
 lacked knowledge about their own ability. Males were also
 unjustifiably over-confident in their abilities.
\end{abstract}

<<echo=FALSE, results=hide>>=
data <- read.csv("../data/cooked/guess.csv")
library(ggplot2)
theme_set(theme_bw())
library(lme4)

truth = c(456,854,1243,4000,8200,2543,310)
          #B  C    D    F    G     E    A
data$risk <- 0
data$risk[data$bonus=="risk"]=1
levels(data$url) <- c(1,2,3,4,5,6,7)
data$pic <- NA
data$pic[data$url==1] <- "B"
data$pic[data$url==2] <- "C"
data$pic[data$url==3] <- "D"
data$pic[data$url==4] <- "F"
data$pic[data$url==5] <- "G"
data$pic[data$url==6] <- "E"
data$pic[data$url==7] <- "A"
data$pic <- factor(data$pic)

data$truth <- 0
data$truth[data$url==1] = 456  #B
data$truth[data$url==2] = 854  #C
data$truth[data$url==3] = 1243 #D
data$truth[data$url==4] = 4000 #F
data$truth[data$url==5] = 8200 #G
data$truth[data$url==6] = 2543  #E
data$truth[data$url==7] = 310   #A

data$pic <- factor(data$pic, levels = c("A", "B","C","D","E","F","G"))

data$error <- abs(data$guess - data$truth)
data$error_rate <- data$error / data$truth

# determines who is in the top of the distribution 
data$top <- NA 
for(i in 1:7){
  med <- quantile(with(data, error[url==i]),.5)
  data$top[data$url==i] <- I(data$error[data$url==i] <=med)
}

# what justifies the ex ante trimming? 
old_data <- data 
data <- subset(data, abs(error_rate) < 10)

data$order = NA
ids <- unique(data$id)
for(id in ids){
  data$order[data$id==id]=order(data$accept_time[data$id==id])
}

data$true_gender <- NA
for(i in ids){
  obs = with(data, gender[id==i])
  if(sum(obs=="Female") > sum(obs=="Male")){
    data$true_gender[data$id==i] <- "Female"
  }
  if(sum(obs=="Female") < sum(obs=="Male")){
    data$true_gender[data$id==i] <- "Male"
  }
}

data$true_age <- NA
for(i in ids){
  data$true_age[data$id==i] = with(data, mean(age[id==i], na.rm=TRUE))
}

dist_kb <- function(x){
  n <- nchar(x)
  v <- as.character(x)
if(n >= 2){
  l = 0 
  for(i in 1:(n-1)){
    s1 = as.numeric(substr(v,i,i))
    s2 = as.numeric(substr(v,i + 1,i+1))
    if(s1==0){
      s1=10
    }
    if(s2==0){
      s2=10
    }
    if(s1==s2){
      d = 0
    } else {
        d = abs(s1-s2)
      }
    l = l + d
  }
 l 
} else {
  0
}
}
@

<<echo=FALSE, results=hide>>=
ank <- read.csv("../data/cooked/anchor.csv")
ank$error <- abs(500 - ank$guess)

# not used in the blog post 
ank$correct <- FALSE
ank$correct[ank$anchor < 500 & ank$relative=="Greater"]=TRUE
ank$correct[ank$anchor > 500 & ank$relative=="Less"]=TRUE
ank$astray <- abs(500 - ank$anchor)
trim <- subset(ank, guess < 2500)
@ 

\setkeys{Gin}{width=0.45\textwidth}

\section{Introduction}
One of the canonical crowdsourcing tasks is eliciting information from
geographically dispersed groups of people. Empirical research into
this kind of work could ultimately improve efficiency and quality. In
the natural sciences, empirical research is often focused on ``model
organisms,'' such as E. coli, the fruit fly, and the nematode
worm. Although not intrinsically interesting, these organisms possess
certain properties that make them especially attractive for research
purposes. 

In this note, I propose a potential model task for empirical research
into computer supported work and conducted two experiments using this
task. The task is the dot-guessing game, where subjects guess the
number of dots in a computer-generated image. I used this task in two
experiments conducted on MTurk. The experiments examined the
side-effects of certain forms of elicitation, the potential of using
type-revealing contract choices to infer ability and/or (justified)
confidence, and the relationships among incentives, effort, and
quality.

\subsection{Previous work} 
Several papers have used online labor markets such as Amazon's
Mechanical Turk (MTurk) to conduct experiments
\cite{kittur2008crowdsourcing, snow2008cheap, sheng2008get}. Horton,
Rand, and Zeckhauser discuss the social science potential of online
experiments in these markets, focusing on how challenges to validity
can be overcome \cite{hortonZeck2010}. There already exists a small
literature on crowdsourcing from a social science perspective
\cite{huberman-crowdsourcing, mason2009fip, horton2010labor}.

\subsection{Main Experimental Findings} 
In general, subjects did remarkably well at the task, confirming the
``wisdom of crowds.'' After excluding some egregiously poor guesses,
the mean guess was consistently better than most individual guesses.
Weighting subjects based on their performance on an initial screening
task improved prediction accuracy substantially, though all of the
gain came from excluding very poor performers.

In the first experiment, I found that subjects were highly susceptible
to ``anchoring effects.'' Before offering a guess, subjects were asked
if the true number of dots was above or below some randomly chosen
number. This above/below number had a strong effect on subsequent
guesses. In the second experiment, I found that offering ``high-powered''
incentive contracts tying payment to performance were largely
ineffective. Uptake of the contract did not reveal which subjects were
actually good at the task. Uptake was, however, correlated with
gender. Both the anchoring result and the gender/risk preference
result have been noted in behavioral economics experiments.

\section{A Model Task?}
It would be convenient to have a single task in experimental studies
that could generate results generalizable to other domains. Such a
task could improve our ability to compound knowledge and lead to more
replications, thereby increasing our confidence in findings. It seems
unlikely that a universally appropriate task exists. However, there
are certain commonalities among tasks and, at least for basic
research, certain tasks might partially fill the model organism role.

We would like a task that is culturally neutral, easy to explain and
lead to hetergeneous quality.  Ideally, quality should be a
nearly-continuous variable, permitting fine-grained distinctions to be
made between outputs. We should be able to generate new, unique tasks
with ease. Any task should---like a CAPTCHA---be a hard AI problem,
especially if we try to use high-powered incentives that might
encourage cheating via algorithms.

\begin{comment}
What are some desirable characteristics for such a task? It should be
culturally neutral and easy to explain, even to subjects with poor
literacy skills. The quality of worker output should be objective and
easy to measure, yet quality should be naturally
heterogeneous. ``Natural'' variation in quality is necessary, not only
in order to make it easy to identify treatment effects from different
manipulations, but also because one of the key challenges in mechanism
design is reliably identifying and overweighting top performers and
reducing the influence of poor performers.
Continuous
measures minimize the chance of ties, making it easier to offer
precise relative-performance contracts or to display subjects'
relative positions on a ``leader board.'' 

%Continuous quality measures
%also simplify analysis by permitting the use of linear models instead
%of the more complex models needed to analyze dichotomous outcomes.  In
%order to investigate learning, the task should allow subjects to
%improve with effort or training. There should also be positive
%relationships between incentives and effort and between effort and
%quality.

Because so many HCOMP tasks deal with aggregating bits of information,
the model task should have a ``wisdom of crowds'' potential. At the
same time, there might also be cases where groupthink, herding, or the
``madness of crowds'' could prevail. Ideally, subjects should be able
to share information and to respond to information about the task
provided by others. To measure group-level performance, work outputs
should be easy to aggregate so that different aggregation approaches
can be tested.

Finally, logistically, we want to be able to generate new, unique
tasks with ease. Any task should---like a CAPTCHA---be a hard AI
problem, especially if we try to use high-powered incentives that
might encourage cheating via algorithms.
\end{comment} 

\subsection{Dot-guessing game} 
In the dot guessing game, subjects are asked to guess the number of
dots in a computer-generated image. Creating new and unique tasks in
any computer language is only marginally more challenging than the
``hello world'' program. The R code for generating such an image is
simply:
\begin{verbatim}
n = 500 
X = c(runif(n,0,1), runif(n,0,1)) 
plot(X,axes=F, xlab="", ylab="")
\end{verbatim} 

Aside from ease of creation, the dot-guessing game has a number of
desirable properties as a research tool.  It is very simple to
explain. It has an unambiguous quality metric---how far off a guess is
from the correct answer---and yet answers cannot simply be looked up
online or solved (easily) with a computer. It is in some ways similar
to the ``pseudo-subjective'' questions psychologists have used to
measure over-confidence, such as asking subjects to estimate the
length of the Nile river \cite{biais2005judgemental}.

\begin{comment}
Obviously the dot-guessing game also has limitations. It is not good
for generative tasks (e.g., image labeling). Nor is it ideal for
studying coordination, where tasks like the graph-coloring problem may
be more appropriate \cite{kearns2006experimental}. However, for a wide
range of human computation scenarios that require judgment by workers
and the screening and aggregation of inputs, the dot-guessing game
offers many advantages.
\end{comment} 

%\begin{figure} 
%<<echo=TRUE, fig=TRUE>>=
%par(mar=c(0,0,0,0))
%n = 500
%X = c(runif(n,0,1), runif(n,0,1))
%plot(X,axes=F, xlab="", ylab="")
%@ 
%\caption{Example dot-guessing game with generating R code.\label{fig:dots}}
%\end{figure} 

\section{Experiment A}
In the first experiment, 200 subjects were recruited to complete a
single HIT.  They each inspected a single image containing 500 dots
and offered a guess. Before proffering a guess, each subject first
answered whether they thought the number of dots was greater than or
less than a number $X$ randomly drawn from a uniform distribution, $X
\sim U[100,1200]$.

<<echo=FALSE, results=hide>>=
mu1 <- with(trim, round(mean(guess)))
med1 <- with(trim, round(quantile(guess,.5)))
gm1 <- with(trim, round(exp(mean(log(guess)))))
n1 <- length(trim$guess)
sd1 <- round(sqrt(var(trim$guess)))
f1 <- with(trim, 1-round(ecdf(abs(guess-500))(abs(mu1-500)),2))
           
Y <- function(x){
  g <- with(data, guess[pic==x])
  truth <- with(data, truth[pic==x])[1]
  mu <- round(mean(g))
  gmu <- round(exp(mean(log(g))))
  med <-round(quantile(g,.5))
  n <- length(g)
  sd <- round(sqrt(var(g)))
  f <-  1-round(ecdf(abs(g-truth))(abs(mu-truth)),2)
  list(mu = mu, gmu = gmu, med = med, n = n, sd = sd, f = f)
}

create_line <- function(x){
  l <- Y(x)
  paste(l$mu, "&", l$gmu,"&", l$med, "&", l$sd, "&", l$n,"&", l$f)
}

@ 

\begin{table} 
  \caption{Dot-guessing performance\label{tab:perf}}  
  \begin{small}
  \begin{center} 
  \begin{tabular}{clcccccc} 
   Dots & & Mean & Geo. Mean & Med. & SD & N & Quant. \\
   \hline
\multicolumn{7}{l}{\emph{Anchor Experiment}} \\   
    500 & & \Sexpr{mu1} & \Sexpr{gm1} & \Sexpr{med1} & \Sexpr{sd1} & \Sexpr{n1} & \Sexpr{f1} \\    
\multicolumn{7}{l}{\emph{Incentive Experiment}} \\   
    310 &(1)&  \Sexpr{create_line('A')} \\
    456 &(2)&  \Sexpr{create_line('B')} \\
    854 &(3)&  \Sexpr{create_line('C')} \\
    1243& (4)& \Sexpr{create_line('D')} \\
    2543 &(5)& \Sexpr{create_line('E')} \\
    4000 &(6)& \Sexpr{create_line('F')} \\
    8200 &(7)& \Sexpr{create_line('G')} \\
    \end{tabular}
  \end{center}
  \end{small}
  \begin{small} 
  \emph{Notes:} Data excludes subjects with error rates greater
  10. ``Quant.'' reports the location of the error associated with the
  mean guess in the distribution of errors associated with all
  guesses.
  \end{small} 
  \end{table} 

<<echo=FALSE>>=
comp_meth <- data.frame(truth = c(500,310,456,854,1243,2543,4000,8200), 
                        mean=c(517,375,537,986,1232,2362,4888,8559), 
                        gmean = c(435,285,373,579,866,1684,2855,4748), 
                        med = c(450,292,400,606,850,1700,2562,4644))
@ 

\subsection{Results}
Overall, subjects performed quite well, with the mean besting most
individual guesses once very bad outliers were removed. Table
\ref{tab:perf} shows the performance results for the``Anchor
Experiment'' (as well as for the follow-on experiment) after removing
subjects with an error \emph{rate} of $10$ or greater. The experiment
highlights the dangers of free-response elicitation: one subject
estimated that the photograph contained \Sexpr{max(ank$guess)}
dots---an over-estimate by a factor of over
\Sexpr{floor(max(ank$guess)/500)}.

\subsubsection{Anchoring effects}
<<echo=FALSE,results=hide>>=
m <- lm(guess ~ anchor, data = trim)
c0 = round(coef(m)[1],3)
se0 = round(sqrt(diag(vcov(m)))[1],2)

c1 = round(coef(m)[2],3)
se1 = round(sqrt(diag(vcov(m)))[2],2)
r2 = round(summary(m)$r.squared,2)
n = length(summary(m)$residuals) 
@ 

Despite the good aggregate performance, subjects' guesses about the
500-dot image were strongly affected by asking whether the number of
dots was above or below the randomly chosen anchors: a 10-point
increase in the anchor increases guesses by approximately 3
points. Regressing guesses on the anchors gives
\[ 
 y_{i} = \underbrace{\Sexpr{c1}}_{\Sexpr{se1}} \cdot X_{i} + \underbrace{\Sexpr{c0}}_{\Sexpr{se0}}
\] 
with $R^2= \Sexpr{r2}$ and $N=\Sexpr{n}$.  Standard errors are shown
underneath the coefficient estimates. Figure \ref{fig:anchor}
illustrates the guess-anchor relationship through a scatter plot of
the logged guesses versus the logged anchors and a local kernel
regression line.
\begin{figure} 
  \begin{center}
   
<<echo=FALSE,fig=TRUE>>=
# removing outliers 
#qplot(anchor,guess,geom=c("point","smooth"), ank = trim)
qq <- qplot(log(anchor), log(guess), ,geom=c("point","smooth"), data = trim)
print(qq)
@ 
\end{center} 
 \caption{Log guesses versus log anchors. Subjects' guesses for a 500-dot
   image are plotted against their anchors.\label{fig:anchor} }
\end{figure} 


\section{Experiment B} 
The second experiment was designed to test whether incentive contracts
reveal information about subject confidence and/or motivate better
performance. Subjects were simultaneously asked to guess the number of
dots in an image and to state their preference between two incentive
contracts: (1) \$5.00 if the subject's answer was in the top half of
all guesses, \$0 otherwise; or (2) \$2.50 for certain. The response
was coded as $risk_{ij}=1$ if the subject chose the incentive
contract, $risk_{ij}=0$ if the subject chose the fixed
contract. Similarly, if a subject $i$ was in the top half of the
distribution for image $j$, then $top_{ij}=1$. Subjects were allowed
to view up to seven images, each of which contained a different number
of dots. Having multiple responses per subject makes a multilevel
model appropriate for analysis \cite{gelman2007data}, and all the
regressions include individual-specific random effects.

\begin{figure} 
  \begin{center}
<<echo=FALSE,fig=TRUE>>=
stat_sum_df <- function(fun, geom="crossbar", ...) { 
   stat_summary(fun.data=fun, colour="red", geom=geom, width=.2, ...) 
} 
g <- ggplot(data, aes(x = log(guess)))
temp <- data.frame(truth = c(310,456,854,1243,2543,4000,8200))
temp$xline <- log(temp$truth)
g <- g + geom_histogram() + geom_vline(aes(xintercept=xline), data = temp, col="red")
g <- g + facet_grid(truth~.)
print(g)
#print(woc_plot)
@ 
\end{center} 
\caption{Distributions of log guesses with correct answers indicated by vertical
  bars.\label{fig:logguess}}
\begin{small} 
\end{small}
\end{figure} 

Subjects were not truly randomly assigned to HITs, but rather were
randomly assigned a HIT from the pool of uncompleted HITs. When a
worker ``returned'' a task (i.e., decided not to complete it), it was
returned to the pool. Over time, the portion of images with high
numbers of dots in the pool increased, as workers were more likely to
return those images. The reasons workers found those images more
burdensome are unclear. The most likely explanation is that the images
simply took longer to download, due to larger file sizes: in a short
follow-on experiment using images of equal file size containing
different numbers of dots, differential attrition disappeared.

To minimize the burden of answering survey questions, subjects were
randomly assigned to be asked one of the following demographic
questions or no question at all: (1) age, (2) gender, or (3) both age
and gender.

\subsection{Results} 
Figure \ref{fig:logguess} shows the histograms of log guesses for each
of the seven images. The logs of the correct answers are shown by
vertical red lines. In general, guesses appear to be log normally
distributed. Among the images with relatively fewer dots, the
distributions appear to be symmetric around the correct answers. The
symmetry breaks down among the images with larger numbers of dots, and
there is evidence of systematic under-guessing combined with a few
very large positive outliers.

\subsubsection{Contract Choice} 
<<echo=FALSE,results=hide>>=
m <- lmer(risk ~ top + (1|id), data = data)
c1 <- sprintf("%.2f",fixef(m)["topTRUE"])
se1 <-sprintf("%.2f",sqrt(diag(vcov(m)))[2])

c0 = sprintf("%.2f", fixef(m)["(Intercept)"])
se0 =sprintf("%.2f", sqrt(diag(vcov(m)))[1])

e = resid(m)
n = length(e)
r2 = sprintf("%.2f",1 - sum((e**2))/sum((data$risk**2)))

i_se = sprintf("%.2f",sqrt(var(ranef(m)$id["(Intercept)"])))
@ 

Few subjects selected to take the incentive contract, with mean uptake
only $\Sexpr{round(100*mean(data$risk),2)}$\%.  Regressing
contract choice on an indicator for being in the top of the
distribution, we have
\[ 
  risk_{ij} = \underbrace{\Sexpr{c1}}_{\Sexpr{se1}} \cdot top_{ij} + \underbrace{\Sexpr{c0}}_{\Sexpr{se0}} + \ldots 
\]
with $R^2 = \Sexpr{r2}$ and $N = \Sexpr{n}$. As with all the
regressions examining the second experiment, this regression includes
picture fixed effects and individual random effects. We can see that
the choice of contract does not appear to reveal anything meaningful
about the quality of the subjects' responses. Unsurprisingly, the
error rate is also unrelated to contract choice. Regressing $E_{ij}$
on the incentive contract indicator and dummy variables for each of
the seven images (coefficeints are omitted below), there is no
apparent relationship
<<echo=FALSE, results=hide>>=
m <- lmer(error_rate ~ risk + pic + (1|id), data = data)

c1 <- sprintf("%.2f",fixef(m)["risk"])
se1 <-sprintf("%.2f",sqrt(diag(vcov(m)))[2])

e = resid(m)
n = length(e)
r2 = sprintf("%.2f",1 - sum((e**2))/sum((data$error_rate**2)))

@ 
\[ 
  E_{ij} = \underbrace{\Sexpr{c1}}_{\Sexpr{se1}} \cdot risk_{ij} + \dots 
\] 
with $R^2 = \Sexpr{r2}$ and $N=\Sexpr{n}$. 
<<echo=FALSE,results=hide>>=
m <- lmer(log(time) ~ risk + pic + (1|id), data = data)

c1 <- sprintf("%.2f",fixef(m)["risk"])
se1 <-sprintf("%.2f",sqrt(diag(vcov(m)))[2])

e = resid(m)
n = length(e)
r2 = sprintf("%.2f",1 - sum((e**2))/sum((log(data$time)**2)))
@ 

As we can see from the regression line, workers who chose the
incentive contract performed slightly worse, but the effect was not
significant. However, those workers did spend more time on the task, 
though the effect was not significant at conventional levels: 
\[ 
  \log time_{ij} = \underbrace{\Sexpr{c1}}_{\Sexpr{se1}} \cdot risk_{ij} + \dots 
\] 
with image-specific fixed effects and individual level random effects,
giving $R^2 = \Sexpr{r2}$ and $N=\Sexpr{n}$. Note that the high $R^2$
in this regression results from the inclusion of the fixed effects of
the image. Because the task itself took so little time, most of the
variation in times is likely the result of differences in download
time. Although in this experiment there was no relationship between
time spent and work quality, this is certainly not a general
result. Adding an incentive contract could be beneficial, even if the
choice of contract is not type-revealing.

\subsection{Gender and contract choice} 
<<echo=FALSE,results=hide>>=
data$contract <- "Risky" 
data$contract[data$risk==0] <- "Safe" 
data$male <- data$true_gender=="Male"
m <- lmer(risk ~ male + pic + (1|id), data = data) 
c1 <- sprintf("%.2f",fixef(m)["maleTRUE"])
se1 <-sprintf("%.2f",sqrt(diag(vcov(m)))[2])
e = resid(m)
n = length(e)
r2 = sprintf("%.2f",1 - sum((e**2))/sum((data$risk**2)))
@ 
Regressing contract choice on gender, and including image-specific
fixed-effects and individual-specific random effects, we have
\[ 
  risk_{ij} = \underbrace{\Sexpr{c1}}_{\Sexpr{se1}} \cdot male_{ij} + \dots 
\] 
with $R^2 = \Sexpr{r2}$ and $N=\Sexpr{n}$. Although both male and
female subjects preferred the fixed contract, females had a stronger
preference for the risk-free contract. This is consistent with
experimental evidence that females are more risk-averse when playing
abstract games.\footnote{However, others have argued that this gender
  difference disappears when decisions are contextualized as real
  business decisions about things like investments and insurance
  \cite{schubert1999dm}.} This finding of gender difference is not in
itself surprising, but it does suggest that using incentive contract
uptake to sort individuals is problematic and could lead to
inefficient, gender-based discrimination.
 
<<echo=FALSE,results=hide>>=
m <- lmer(error_rate~ male + pic + (1|id), data = subset(data, !is.na(true_gender)))
c1 <- sprintf("%.2f",fixef(m)["maleTRUE"])
se1 <-sprintf("%.2f",sqrt(diag(vcov(m)))[2])
e = resid(m)
n = length(e)
r2 = sprintf("%.2f",1 - sum((e**2))/sum((data$error_rate**2)))
@ 
In fact, risk-seeking male subjects actually
performed slightly worse than females, although the effect is not
significant
\[ 
  E_{ij} = \underbrace{\Sexpr{c1}}_{\Sexpr{se1}} \cdot male_{ij} + \dots 
\] 
with image fixed effects and individual random effects, giving $R^2 = \Sexpr{r2}$ and $N=\Sexpr{n}$.

<<echo=FALSE,results=hide>>=
data$q <- NA 
for (i in ids){
# first picture they guessed on
  url_id = with(data, url[id==i & order==1])
  err = with(data, error[id==i & order==1])
# error distribution
  f <- with(data, ecdf(error[url==url_id]))
# the worker's quantile in the error distribution   
  data$q[data$id==i] <- f(err) 
}
#m <- lm(error_rate ~ q, data = subset(data, order!=1))
@ 

\section{Conclusion} 
The results generally confirm the ``wisdom of crowds'' hypothesis.  By
excluding the worst performers, the mean guesses were quite accurate,
though performance worsened as the number of dots in the image
increased. Subjects were prone to the anchoring bias and they do not
reveal their type (i.e., relative aptitude for the task) through their
preference in contract type. Males were more likely to choose an
incentive contract, but they performed no better than females.  There
is some evidence that subjects who chose the incentive contract spent
more time on the tasks, though it is unclear which way causality ran.

%Ex post
%processing of the data improved prediction accuracy: removing the
%worst 5\% of subjects led to improvements in accuracy, reducing the
%error in six of the seven cases (and essentially equalizing the
%results of the seventh case).

%\subsection{Implications}
%The results suggest that mechanism designers should consider that: (a)
%choice of contract type may not be informative, and (b) differences in
%risk appetite (which may be systematic and/or have a gender component)
%may lead to mechanisms that inefficiently overweight the
%over-confident. Because I found a positive correlation between time
%spent and preference for an incentive contract, it may be the case
%that incentive contracts improve quality, though contract choice was
%endogenous, as was the time spent on the task.

%The anchoring results suggest that mechanism designers must be
%cautious in querying subjects, even in a hypothetical manner. It seems
%possible that subjects will accept those questions as informative and
%adjust their beliefs rather than treat them as irrelevant (in the
%sense of not providing new information).

\begin{comment}
\subsection{Case for Empiricism} 
As more aspects of social life moves online, both the size of collected
observational data and the potential to conduct experiments will
continue to grow. The transition to a bench science, where hypotheses
can be quickly tested through experimentation, is bound to have
positive effects.

The case for empiricism is particularly strong in designing
mechanisms, as theory offers little guidance about cognitive demands on
the mechanism, how contextual factors
are likely to influence subjects, how boundedly rational workers actually play,
etc. To make practically useful mechanisms, it is important to
understand how they hold up when the players are real people, prone to
the biases and subject to the limitations of real people \cite{mcfadden2009human}.
\end{comment}

\section{Acknowledgments} 
Thanks to the the Christakis Lab at Harvard Medical School and the
NSF-IGERT Multidisciplinary Program in Inequality \& Social Policy for
generous financial support (Grant No. 0333403). Thanks to Robin Yerkes
Horton, Brendan O'Connor, Carolyn Yerkes, and Richard Zeckhauser for
very helpful comments. All plots were made using ggplot2
\cite{wickham2008ggplot2} and all multilevel models were fit using
\texttt{lme4}\cite{bates2008lme4}.

\bibliographystyle{aer}
\bibliography{guessing_game.bib}

\end{document}   

