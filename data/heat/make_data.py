
import csv, datetime, calendar, time 

amt = open("../raw/amt_results.csv", 'r')

raw = csv.reader(amt, delimiter=",")
head = raw.next()
hd = dict(zip(head,range(len(head))))

extract = ['WorkerId','WorkTimeInSeconds', 
'Input.url', 'Answer.bonus', 'Answer.guess', 
'Answer.gender_question', 'Answer.age_question', 'Answer.age', 'Answer.gender', 'AcceptTime'] 

new_names = ["id", "time", "url", "bonus", "guess", "genderT", "ageT", "age", "gender", "accept_time"]

def epochify(time_list):
    tmp = [calendar.timegm(time.strptime(t,'%a %b %d %H:%M:%S %Z %Y')) for t in time_list]
    first = min(tmp)
    return [y - first for y in tmp] 

data = []
for line in raw:
    values = [line[hd[y]] for y in extract]
    data.append(values) 

epoch = epochify([y[9] for y in data])

cooked = csv.writer(open("../cooked/guess.csv", 'w'), delimiter=",")
cooked.writerow(new_names)

for line,t in zip(data, epoch):
    l = line[0:9]
    l.append(t)
    print l
    cooked.writerow(l)
